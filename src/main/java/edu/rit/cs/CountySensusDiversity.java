package edu.rit.cs;

import java.io.Serializable;

/**
 * Represents a high level abstraction used for the diversity index calculation
 */
public class CountySensusDiversity implements Serializable {
    private String stateName;
    private String countyName;

    private int whitePopulation;
    private int blackPopulation;
    private int nativePopulation;
    private int asianPopulation;
    private int nativeHawaiinPopulation;
    private int mixedPopulation;

    @Override
    public String toString() {
        return String.format("%s - %s: %d total", this.stateName, this.countyName, this.totalPopulation());
    }

    /**
     * Get the total number of people in this county
     */
    public int totalPopulation() {
        return this.whitePopulation + this.blackPopulation + this.nativePopulation +
                this.asianPopulation + this.nativeHawaiinPopulation + this.mixedPopulation;
    }

    /**
     * Calculate the diversity index for this county
     * @return The diversity index for this county
     */
    public double calculateDiversityIndex() {
        double diversityTotal = 0;
        double totInvSquared = 1.0 / ((double)this.totalPopulation() * this.totalPopulation());

        diversityTotal += (this.whitePopulation * totInvSquared) * (this.totalPopulation() - this.whitePopulation);
        diversityTotal += (this.blackPopulation * totInvSquared) * (this.totalPopulation() - this.blackPopulation);
        diversityTotal += (this.nativePopulation * totInvSquared) * (this.totalPopulation() - this.nativePopulation);
        diversityTotal += (this.asianPopulation * totInvSquared) * (this.totalPopulation() - this.asianPopulation);
        diversityTotal += (this.nativeHawaiinPopulation * totInvSquared) * (this.totalPopulation() - this.nativeHawaiinPopulation);
        diversityTotal += (this.mixedPopulation * totInvSquared) * (this.totalPopulation() - this.mixedPopulation);

        double diversityIndex = diversityTotal;
        if (diversityIndex < 0) {
            System.out.printf("%s : %d : %d : %d : %d : %d : %d : %f : %f\n", this.toString(), this.whitePopulation,
                    this.blackPopulation, this.nativePopulation, this.asianPopulation,
                    this.nativeHawaiinPopulation, this.mixedPopulation, totInvSquared, (this.whitePopulation / totInvSquared) * (this.totalPopulation() - this.whitePopulation));
        }

        return diversityIndex;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getCountyName() {
        return countyName;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }

    public int getWhitePopulation() {
        return whitePopulation;
    }

    public void setWhitePopulation(int whitePopulation) {
        this.whitePopulation = whitePopulation;
    }

    public int getBlackPopulation() {
        return blackPopulation;
    }

    public void setBlackPopulation(int blackPopulation) {
        this.blackPopulation = blackPopulation;
    }

    public int getNativePopulation() {
        return nativePopulation;
    }

    public void setNativePopulation(int nativePopulation) {
        this.nativePopulation = nativePopulation;
    }

    public int getAsianPopulation() {
        return asianPopulation;
    }

    public void setAsianPopulation(int asianPopulation) {
        this.asianPopulation = asianPopulation;
    }

    public int getNativeHawaiinPopulation() {
        return nativeHawaiinPopulation;
    }

    public void setNativeHawaiinPopulation(int nativeHawaiinPopulation) {
        this.nativeHawaiinPopulation = nativeHawaiinPopulation;
    }

    public int getMixedPopulation() {
        return mixedPopulation;
    }

    public void setMixedPopulation(int mixedPopulation) {
        this.mixedPopulation = mixedPopulation;
    }
}
