package edu.rit.cs;

import org.apache.avro.generic.GenericData;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.DataTypes;
import scala.Tuple2;

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;


public class CountySensusSpark {

    /**
     * Print out all the results of the diversity index calculations
     *
     * @param rawResults The results to print
     */
    private static void displayResults(List<Tuple2<String, Double>> rawResults) {
        List<Tuple2<String, Double>> results = new ArrayList<>(rawResults);
        results.sort((r1, r2) -> r1._1.compareTo(r2._1));

        for (Tuple2<String, Double> result : results) {
            String[] regionComponents = result._1.split("@");
            String state = regionComponents[0];
            String county = regionComponents[1];

            System.out.printf("[%s, %s, %.16g]\n", state, county, result._2);
        }
    }

    /**
     * Load the data set file into a list of CountySensus info
     *
     * @param spark The spark session needed for reading the file
     * @param dataFilePath The path to the data file to read
     * @return The parsed data file
     */
    private static Dataset<CountySensus> loadDataset(SparkSession spark, String dataFilePath) {
        Dataset ds = spark.read()
                .option("header", "true")
                .option("delimiter", ",")
                .option("inferSchema", "true")
                .csv(dataFilePath);

        // select out only the columns that I need
        ds = ds.withColumn("STNAME", ds.col("STNAME").cast(DataTypes.StringType))
                .withColumn("CTYNAME", ds.col("CTYNAME").cast(DataTypes.StringType))
                .withColumn("YEAR", ds.col("YEAR").cast(DataTypes.IntegerType))
                .withColumn("AGEGRP", ds.col("AGEGRP").cast(DataTypes.IntegerType))
                .withColumn("WA_MALE", ds.col("WA_MALE").cast(DataTypes.IntegerType))
                .withColumn("WA_FEMALE", ds.col("WA_FEMALE").cast(DataTypes.IntegerType))
                .withColumn("BA_MALE", ds.col("BA_MALE").cast(DataTypes.IntegerType))
                .withColumn("BA_FEMALE", ds.col("BA_FEMALE").cast(DataTypes.IntegerType))
                .withColumn("IA_MALE", ds.col("IA_MALE").cast(DataTypes.IntegerType))
                .withColumn("IA_FEMALE", ds.col("IA_FEMALE").cast(DataTypes.IntegerType))
                .withColumn("AA_MALE", ds.col("AA_MALE").cast(DataTypes.IntegerType))
                .withColumn("AA_FEMALE", ds.col("AA_FEMALE").cast(DataTypes.IntegerType))
                .withColumn("NA_MALE", ds.col("NA_MALE").cast(DataTypes.IntegerType))
                .withColumn("NA_FEMALE", ds.col("NA_FEMALE").cast(DataTypes.IntegerType))
                .withColumn("TOM_MALE", ds.col("TOM_MALE").cast(DataTypes.IntegerType))
                .withColumn("TOM_FEMALE", ds.col("TOM_FEMALE").cast(DataTypes.IntegerType));

        Encoder<CountySensus> reviewEncoder = Encoders.bean(CountySensus.class);
        Dataset<CountySensus> datasetParsed = ds.as(reviewEncoder);

        return datasetParsed;
    }

    /**
     * Compute the diversity index for each state specified
     *
     * @param spark The spark session
     * @param stateNames The names of the states to calculate the diversity index of
     * @param dataFilePath The path to the data file containing the population information
     * @return A list of counties with their diversity indexes
     */
    private static List<Tuple2<String, Double>> computeUsDiversityIndices(SparkSession spark,
                                                                          List<String> stateNames,
                                                                          String dataFilePath) {
        Dataset<CountySensus> dataset = loadDataset(spark, dataFilePath);

        // if states are specified, only consider those ones
        JavaRDD<CountySensus> statesToConsider = dataset.toJavaRDD();
        if (stateNames.size() > 0) {
            statesToConsider = statesToConsider.filter(c -> stateNames.contains(c.getSTNAME()));
        }

        // only keep the entries that sum over all age groups
        JavaRDD<CountySensus> filteredAgeGroup0 = statesToConsider.filter(c -> c.getAGEGRP() == 0);

        // combine all of the male and female data
        JavaRDD<CountySensusDiversity> mfCombined = filteredAgeGroup0.map(c -> {
            CountySensusDiversity countyDiversity = new CountySensusDiversity();
            countyDiversity.setStateName(c.getSTNAME());
            countyDiversity.setCountyName(c.getCTYNAME());
            countyDiversity.setWhitePopulation(c.getWA_MALE() + c.getWA_FEMALE());
            countyDiversity.setBlackPopulation(c.getBA_MALE() + c.getBA_FEMALE());
            countyDiversity.setNativePopulation(c.getIA_MALE() + c.getIA_FEMALE());
            countyDiversity.setAsianPopulation(c.getAA_MALE() + c.getAA_FEMALE());
            countyDiversity.setNativeHawaiinPopulation(c.getNA_MALE() + c.getNA_FEMALE());
            countyDiversity.setMixedPopulation(c.getTOM_MALE() + c.getTOM_FEMALE());

            return countyDiversity;
        });

        // create a key value pair of "region -> census data"
        JavaPairRDD<String, CountySensusDiversity> kvCounty2Data = mfCombined.mapToPair(cd -> {
            return new Tuple2<>(cd.getStateName() + "@" + cd.getCountyName(), cd);
        });

        // combine all of the cities in the same county
        JavaPairRDD<String, CountySensusDiversity> combinedCities = kvCounty2Data.reduceByKey((cd1, cd2) -> {
            CountySensusDiversity combinedDiversity = new CountySensusDiversity();
            combinedDiversity.setStateName(cd1.getStateName());
            combinedDiversity.setCountyName(cd1.getCountyName());

            assert (cd1.getCountyName().equals(cd2.getCountyName()));

            combinedDiversity.setWhitePopulation(cd1.getWhitePopulation() + cd2.getWhitePopulation());
            combinedDiversity.setBlackPopulation(cd1.getBlackPopulation() + cd2.getBlackPopulation());
            combinedDiversity.setNativePopulation(cd1.getNativePopulation() + cd2.getNativePopulation());
            combinedDiversity.setAsianPopulation(cd1.getAsianPopulation() + cd2.getAsianPopulation());
            combinedDiversity.setNativeHawaiinPopulation(cd1.getNativeHawaiinPopulation() + cd2.getNativeHawaiinPopulation());
            combinedDiversity.setMixedPopulation(cd1.getMixedPopulation() + cd2.getMixedPopulation());

            return combinedDiversity;
        });

        // calculate the diversity index for all of the counties
        JavaRDD<Tuple2<String, Double>> results = combinedCities.map(tuple -> {
            return new Tuple2<>(tuple._1, tuple._2.calculateDiversityIndex());
        });

        return results.collect();
    }

    public static void main(String[] args) throws Exception {
        if (args.length < 1) {
            System.out.println("Usage: dataFilePath [states]");
            System.exit(1);
        }
        MyTimer timer = new MyTimer("SparkApplication");
        timer.start_timer();

        String dataFilePath = args[0];
        List<String> states = new ArrayList<>(Arrays.asList(args).subList(1, args.length));

        // Create a SparkConf that loads defaults from system properties and the classpath
        SparkConf sparkConf = new SparkConf();
        sparkConf.set("spark.master", "local[4]");
        //Provides the Spark driver application a name for easy identification in the Spark or Yarn UI
        sparkConf.setAppName("Diversity Index");

        // Creating a session to Spark. The session allows the creation of the
        // various data abstractions such as RDDs, DataFrame, and more.
        SparkSession spark = SparkSession.builder().config(sparkConf).getOrCreate();

        // Creating spark context which allows the communication with worker nodes
        JavaSparkContext jsc = new JavaSparkContext(spark.sparkContext());

        List<Tuple2<String, Double>> rawResults = computeUsDiversityIndices(spark, states, dataFilePath);
        displayResults(rawResults);

        // Stop existing spark context
        jsc.close();

        // Stop existing spark session
        spark.close();

        timer.stop_timer();
        timer.print_elapsed_time();
    }
}
